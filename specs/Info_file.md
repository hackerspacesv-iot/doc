# Node Capabilities Description File
The content of this file is published on the +/info topic. And describes the
capabilities of a node joining the network.

## Format example:
```json
{
  'name': "nodename",
  'input': {
		'b': ['light0','light1'],
    'f': ['speed']
  },
	'output': {
		'b': ['door_switch']
	},
	'udp': {
		'led_stripe': 5543,
    'ntp': 5555
  },
  'tcp': {
		'tftp': 69
  }
}
```

